#!/bin/bash

set -e

# Bump chart version and commit changes
# This script requires following tools to be installed:
# * https://github.com/mbenabda/helm-local-chart-version
# * https://github.com/norwoodj/helm-docs

function log() {
  echo -e "\033[1;33m$1\033[0m"
}

version_segment="${1:-patch}"
chart="dependabot-gitlab"

log "bump chart version"
helm local-chart-version bump -c "charts/$chart" -s "$version_segment"

log
log "update docs"
helm-docs -c charts -o ../../README.md

log
log "create release commit"
version="v$(helm local-chart-version get -c charts/$chart)"
git commit -a -m "Update chart to version $version"
git tag "$version"
